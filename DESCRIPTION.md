## Principles, Patterns and PSR Proposals
#### SOLID, DRY and KISS principles, Design Patterns and PSR's proposals

**This project is under development**

**Code is being implemented by using SOLID, DRY and KISS principles, Design Patterns, FIG proposals (PSR's), TDD and all those things that old juniors hate.**

The goal is something simple, but useful, respecting the conventions and good practices to make it easy maintanable. It was used **npm** and **composer** to manage package and dependencies, respectively. **PSR-1** and **PSR-2** for code conventions, **PSR-3** for logger interface and **PSR-4** for autoloading. It's being used stream wrappers for session storage, temporary data storage and to test http requests. All of the code is being developed by following the **TDD** approach with **PHPUnit Framework**. **JS** scripts is being tested, by implementing **BDD** approach with **Jasmine Framework**. Project code is being documented by using **phpDocumentor**.


*by Thiago Mallon*
